<?php

# MIT License
#
# Copyright (c) 2017 Cyrille Georgel < cyrille@bluerocktel.com >
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# Works with a table 'telephonyCallsDirect':

# CREATE TABLE `telephonyCallsDirect` (
#   `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
#   `callInfo` text,
#   `nichandle` varchar(55) NOT NULL DEFAULT '',
#   `billingAccount` varchar(55) NOT NULL,
#   `dossierID` int(11) NOT NULL,
#   `phoneLine` varchar(55) NOT NULL,
#   `callID` bigint(20) NOT NULL,
#   `calling` varchar(55) NOT NULL,
#   `priceWithoutTax` decimal(9,4) DEFAULT NULL,
#   `duration` int(11) NOT NULL,
#   `designation` varchar(255) DEFAULT NULL,
#   `countrySuffix` varchar(255) DEFAULT NULL,
#   `destinationType` varchar(55) NOT NULL,
#   `creationDatetime` datetime NOT NULL,
#   `wayType` varchar(55) NOT NULL,
#   `called` varchar(55) NOT NULL,
#   `insertDate` datetime NOT NULL,
#   PRIMARY KEY (`id`),
#   KEY `phoneLine` (`phoneLine`),
#   KEY `destinationType` (`destinationType`),
#   KEY `wayType` (`wayType`),
#   KEY `creationDatetime` (`creationDatetime`),
#   KEY `callID` (`callID`)
# );

include_once 'config.php';

include_once 'pdo.php';

require_once ABSPATH . '/vendor/autoload.php';

use \Ovh\Api;


Class TelcoDirectCallsOVH extends Api
{

    private $dateToGet;
    private $executionMode;


    /**
     * Set the date to get
     * @param string date in yyyy-mm-dd format
     */
     public function setDateToGet($dateToGet)
    {
        $this->dateToGet = $dateToGet;
    }

    /**
     * Get the date to get
     * @param string date in yyyy-mm-dd
     * @return a DateTime object
     */
    private function getDateToGet()
    {
        return new DateTime($this->dateToGet);
    }

    /**
     * Set the execution mode (verbose or silent)
     * @param string execution mode
     */
    public function setExecutionMode($executionMode)
    {
        $this->executionMode = $executionMode;
    }


    /**
     * Get the execution mode (verbose or silent)
     * @param string execution mode
     */
    private function getExecutionMode()
    {
        return $this->executionMode;
    }

    /**
     * Set the API method (previousVoiceConsumption or voiceConsumption)
     * @param string date
     * @return string method
     */
     private function setAPIMethod($date)
     {
        $today = new DateTime();

        if($date->format('m') < $today->format('m'))
        {
            $method = 'previousVoiceConsumption';
        }
        else
        {
            $method = 'voiceConsumption';
        }

        return $method;
    }


    /**
     * Check if the call already exists in the table
     * @param int callID
     * @return boolean
     */
    private function callExists($callID)
    {
        global $dbh;

        $sth = $dbh->prepare("SELECT COUNT(id) as nb
        FROM telephonyCallsDirect
        WHERE callID = ?");

        $sth->execute(array(
            $callID
        ));

        $data = $sth->fetchObject();

        if($data->nb > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    /**
     * Insert the call into the table
     * @param int callID
     * @return boolean
     */
    private function insertCall($callDetails, $nichandle, $billingAccount, $service, $callID)
    {
        global $dbh;

        $sth = $dbh->prepare("INSERT INTO telephonyCallsDirect
        (callInfo, nichandle, billingAccount, phoneLine, callID, calling,
        priceWithoutTax, duration, designation, countrySuffix,
        destinationType, creationDatetime, wayType, called, insertDate)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

        $callInfo = json_encode($callDetails, JSON_PRETTY_PRINT);

        if(isset($callDetails['calling']) || $callDetails['calling'] != '')
        {
            $calling = $callDetails['calling'];
        }
        else
        {
            $calling = '00';
        }

        $sth->execute(array(
            $callInfo,
            $nichandle,
            $billingAccount,
            $service,
            $callID,
            $calling,
            $callDetails['priceWithoutTax']['value'],
            $callDetails['duration'],
            $callDetails['designation'],
            $callDetails['countrySuffix'],
            $callDetails['destinationType'],
            $callDetails['creationDatetime'],
            $callDetails['wayType'],
            $callDetails['called'],
            date('Y-m-d H:i:s')
        ));

        if($sth->rowCount() == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    /**
     * Run the process...
     */
    public function run()
    {

        $scriptStart = time();

        $nbCalls = 0;
        $nbInsert = 0;

        $nichandle = $this->get('/me')['nichandle'];

        $dateToGet = $this->getDateToGet();

        $executionMode = $this->getExecutionMode();

        $dateTo = new DateTime($dateToGet->format('Y-m-d') . ' +1day');

        $apiMethod = $this->setAPIMethod($dateToGet);

        $billingAccounts = $this->get('/telephony');

        foreach($billingAccounts as $billingAccount)
        {
            try
            {
                $services = $this->get('/telephony/' . $billingAccount . '/service');

                foreach($services as $service)
                {
                    try
                    {
                        $calls = $this->get('/telephony/' . $billingAccount
                        . '/service/' . $service .'/' . $apiMethod
                        . '?creationDatetime.from=' . $dateToGet->format('Y-m-d')
                        . '&creationDatetime.to=' . $dateTo->format('Y-m-d'));

                        foreach($calls as $callID)
                        {
                            $nbCalls++;

                            if(!$this->callExists($callID))
                            {
                                try
                                {
                                    $callDetails = $this->get('/telephony/'
                                    . $billingAccount . '/service/' . $service
                                    . '/' . $apiMethod . '/' . $callID);

                                    if(!$this->insertCall($callDetails, $nichandle, $billingAccount, $service, $callID))
                                    {
                                        if($executionMode == 'verbose')
                                        {
                                            print 'insert call ' . $callID . ' KO' . PHP_EOL;
                                        }
                                    }
                                    else
                                    {
                                        $nbInsert++;

                                        if($executionMode == 'verbose')
                                        {
                                            print 'insert call ' . $callID . ' OK. ' . $nbCalls . ' examined and ' . $nbInsert . ' calls inserted so far' . PHP_EOL;
                                        }


                                    }
                                }
                                catch (Exception $e)
                                {
                                    print_r( $e->getMessage() );
                                }
                            }
                            else
                            {
                                if($executionMode == 'verbose')
                                {
                                    print $callID . ' was already recorded in the table. ' . $nbCalls . ' examined so far.' . PHP_EOL;
                                }
                            }
                        }
                    }
                    catch (Exception $e)
                    {
                        print_r( $e->getMessage() );
                    }
                }
            }
            catch (Exception $e)
            {
                print_r( $e->getMessage() );
            }
        }

        $scriptEnd = time();

        $executionTime = $scriptEnd - $scriptStart;

        print PHP_EOL;

        print 'Number of calls examined for date ' . $dateToGet->format('d/m/Y') . ': ' . $nbCalls . '' . PHP_EOL;

        print 'Number of calls inserted for date ' . $dateToGet->format('d/m/Y') . ': ' . $nbInsert . '' . PHP_EOL;

        print 'Execution time for process: ' . $executionTime . '' . PHP_EOL;

        print 'End of process. ' . PHP_EOL;
    }
}

## usage:

## get one specific day:

# $directCalls = new TelcoDirectCallsOVH(APP_KEY, APP_SECRET, APP_ENDPOINT, APP_CONSKEY);
# $directCalls->setExecutionMode('verbose');
# $directCalls->setDateToGet('2017-07-15');
# $directCalls->run();

## get the last three days:

# for($i=1; $i <= 3; $i++)
# {
#     $day = new DateTime(-$i . ' days');
#     $day = $day->format('Y-m-d');
#
#     $directCalls = new TelcoDirectCallsOVH(APP_KEY, APP_SECRET, APP_ENDPOINT, APP_CONSKEY);
#     $directCalls->setExecutionMode('silent');
#     $directCalls->setDateToGet($day);
#     $directCalls->run();
# }

?>
