
# TelcoDirectCallsOVH - Get informations about your calls

TelcoDirectCallsOVH allows you to get details about your calls from OVH API.

## PHP wrapper of OVH API

The current work is based on the lightweight PHP wrapper for OVH APIs,
that you should install first.

Learn more at: https://github.com/ovh/php-ovh

## Usage:

Getting one specific day:

```
$directCalls = new TelcoDirectCallsOVH(APP_KEY, APP_SECRET, APP_ENDPOINT, APP_CONSKEY);
$directCalls->setExecutionMode('verbose');
$directCalls->setDateToGet('2017-07-15');
$directCalls->run();
```

Getting the last three days:

```
for($i=1; $i <= 3; $i++)
{
    $day = new DateTime(-$i . ' days');
    $day = $day->format('Y-m-d');
    $directCalls = new TelcoDirectCallsOVH(APP_KEY, APP_SECRET, APP_ENDPOINT, APP_CONSKEY);
    $directCalls->setExecutionMode('silent');
    $directCalls->setDateToGet($day);
    $directCalls->run();
}
```

## License

TelcoDirectCallsOVH is released under the MIT license. See the bundled LICENSE file for details.

## Also see

OVHBills which allow you to get your OVH bills the easy way.
https://gitlab.com/BlueRockTEL/OVHBills

## Author  

Cyrille Georgel < cyrille@bluerocktel.com >

Also, at BlueRockTEL (http://bluerocktel.com), We help telecom companies billing efficiently and increasing their cash flow. We also provide them with relevant analytics.
