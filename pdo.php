<?php

include_once 'config.php';

$dbServer = DB_SERVER;
$dbPort = DB_PORT;
$dbName = DB_NAME;
$dbUsername = DB_USERNAME;
$dbPassword = DB_PASSWORD;
$dbCharset = DB_CHARSET;

try
{
	$dsn = "mysql:host={$dbServer};port={$dbPort};dbname={$dbName};charset={$dbCharset}";
	$dbh = new PDO($dsn, DB_USERNAME, DB_PASSWORD);
	$dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING ); # debug
	# $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); //prod
}
catch (PDOException $e)
{
	print $e->getMessage();
}

?>
