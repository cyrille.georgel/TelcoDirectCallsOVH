<?php

# OVH API PARAMETERS:
define('APP_ENDPOINT', 'ovh-eu');
define('APP_KEY', '');
define('APP_SECRET', '');
define('APP_CONSKEY', '');

# DB PARAMETERS:
define('DB_SERVER', '');
define('DB_PORT', '');
define('DB_NAME', '');
define('DB_USERNAME', '');
define('DB_PASSWORD', '');
define('DB_CHARSET', 'utf8');

?>
